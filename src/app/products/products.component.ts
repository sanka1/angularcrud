import { Component, OnInit } from '@angular/core';
import {ApiService} from '../api.service';
import {Product} from '../product';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
displayedColumns: string[] = ['prod_name', 'prod_price'];
data: Product[] = [];
isLoadingReults = true;
  constructor(
    private api: ApiService
  ) { }

  ngOnInit() {
    this.api.getProducts().subscribe(res => {
      this.data = res;
      console.log(this.data);
      this.isLoadingReults = false;
    }, err => {
      console.log(err);
      this.isLoadingReults = false;
    });
  }

}
