import { Component, OnInit } from '@angular/core';
import {ApiService} from '../api.service';
import {FormGroup, FormGroupDirective, FormBuilder, FormControl, NgForm, Validator} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {
  productForm: FormGroup;
  prod_name: string ='';
  prod_desc: string ='';
  prod_price: number = null;
  updated_at: Date = null;
  isLoadingResults = false;
  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit() {
   this.productForm = this.formBuilder.group({
     'prod_name' : [null, Validator.required],
     'prod-desc' : [null, Validator.required],
     'prod-price' : [null, Validator.required],
     'updated_at' : [null, Validator.required]
   });
  }
 onFormSubmit(form: NgForm) {
    this.isLoadingResults = true;
    this.api.addProduct(form).subscribe(res => {
      let id = res['_id'];
      this.isLoadingResults = false;
      this.router.navigate(['/product-details', id]);
    }, (err) => {
      console.log(err);
      this.isLoadingResults = false;
    });
 }
}
