import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductAddComponent } from './product-add/product-add.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
// REST API
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import {MatFormField, MatInputModule, MatPaginatorModule, MatProgressSpinnerModule} from '@angular/material';
import {MatSortModule} from '@angular/material/typings/sort';
import {MatTableModule} from '@angular/material/typings/table';
import {MatIconModule} from '@angular/material/typings/icon';
import {MatButtonModule} from '@angular/material/typings/button';
import {MatCardModule} from '@angular/material/typings/esm5/card';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProductDetailComponent,
    ProductAddComponent,
    ProductEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormField
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
