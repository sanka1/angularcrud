import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import {HttpErrorResponse} from '@angular/common/http';
import {catchError, tap, map} from 'rxjs/operators';
import {Product} from './product';
import {Observable, of} from 'rxjs';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = '/api/v1/products';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(apiUrl).pipe(
      tap(heroes => console.log('fetched products')),
      catchError(this.handleError('getProducs', []))
    );
  }

  getProduct(id: number): Observable<Product> {
    const url = '${apiUrl}/${id}';
    return this.http.get<Product>(url).pipe(
      tap(_ => console.log('fetched product id=${id}')),
      catchError(this.handleError<Product>('getProduct id = ${id}'))
    );
  }

  addProduct(product): Observable<Product> {
    return this.http.post<Product>(apiUrl, product, httpOptions).pipe(
      tap((product1: Product) => console.log('added product w/ id=${product.id}')),
      catchError(this.handleError<Product>('addProduct'))
    );
  }

  updateProduct(id, product): Observable<any> {
    const url = '${apiUrl}/${id}';
    return this.http.put(url, product, httpOptions).pipe(
      tap(_ => console.log('updated product id=${id}')),
      catchError(this.handleError<any>('updateProduct'))
    );
  }

  deleteProduct(id): Observable<Product> {
    const url = '${apiUrl}/${id}';

    return this.http.delete<Product>(url, httpOptions).pipe(
      tap(_ => console.log('deleted product id=${id}')),
      catchError(this.handleError<Product>('deleteProduct'))
    );
  }

}

